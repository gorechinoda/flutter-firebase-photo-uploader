import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class Uploader extends StatefulWidget {

  final File file;

  Uploader({this.file});

  @override
  _UploaderState createState() => _UploaderState();
}

class _UploaderState extends State<Uploader> {

  final FirebaseStorage _storage = FirebaseStorage(
    storageBucket: 'gs://photo-upload-it.appspot.com',
  );

  StorageUploadTask _storageUploadTask;

  void _startUpload(){
    String filePath = 'images/${DateTime.now()}';
    setState(() {
      _storageUploadTask = _storage.ref().child(filePath).putFile(widget.file);
    });
  }

  @override
  Widget build(BuildContext context) {
    if(_storageUploadTask != null){
      return StreamBuilder<StorageTaskEvent>(
        stream: _storageUploadTask.events,
        builder: (context,snapshot){
          var event = snapshot?.data?.snapshot;
          double progressPercent = event !=null
              ? event.bytesTransferred/event.totalByteCount
              : 0;

          return Column(
            children: <Widget>[
              if(_storageUploadTask.isComplete)
                Text("Done"),

              if(_storageUploadTask.isPaused)
                FlatButton(
                  child: Icon(Icons.play_arrow),
                  onPressed: _storageUploadTask.resume,
                ),
              if(_storageUploadTask.isInProgress)
                FlatButton(
                  child: Icon(Icons.pause),
                  onPressed: _storageUploadTask.pause,
                ),

              LinearProgressIndicator(value: progressPercent,),
              Text(
                '${(progressPercent * 100).toStringAsFixed(2)}%'
              ),
            ],
          );
        },
      );
    }else{
      return FlatButton.icon(
          onPressed: _startUpload,
          icon: Icon(Icons.cloud_upload),
          label: Text('Upload to firebase'),
      );
    }
  }
}
