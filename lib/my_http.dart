import 'package:http/http.dart' as http;
import 'package:http/http.dart';

const String baseUrl = "http://192.168.43.154:8000/api/";

Future<Response> myGet(String url) {
  print(baseUrl + url);
  return http.get(baseUrl + url);
}

Future<Response> post(String url, data) {
  return http.post(baseUrl + url, body: data);
}

Future<Response> put(String url, data) {
  return http.put(baseUrl + url, body: data);
}

Future<Response> myDelete(String url) {
  return http.delete(baseUrl + url);
}
